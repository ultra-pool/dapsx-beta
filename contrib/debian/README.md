
Debian
====================
This directory contains files used to package DAPSXd/DAPSX-qt
for Debian-based Linux systems. If you compile DAPSXd/DAPSX-qt yourself, there are some useful files here.

## DAPSX: URI support ##


DAPSX-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install DAPSX-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your DAPSXqt binary to `/usr/bin`
and the `../../share/pixmaps/DAPSX128.png` to `/usr/share/pixmaps`

DAPSX-qt.protocol (KDE)

